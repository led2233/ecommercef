<?php 
include '../extend/headerphp.php';
include '../extend/alertas.php';
$clave = htmlentities($_GET['clave']);
$foto = htmlentities($_GET['foto']);
$pagina = htmlentities($_GET['pag']);

if ($pagina == "categorias.php") {
	$opc = htmlentities($_GET['opc']);
	$pagina = $pagina.'?opc='.$opc;
}


$del = $con->prepare("DELETE FROM inventario WHERE clave = :clave");
    $del->bindParam(':clave', $clave);

    if ($del->execute()) {
        if ($foto !='foto_producto/producto.png') {
            unlink($foto);
        }

        $sel = $con->prepare("SELECT ruta FROM imagenes WHERE clave_producto = ?");
        $sel->execute(array($clave));
        while ($f = $sel->fetch()) {
            unlink($f['ruta']);
        }
        $sel = null;

        $dell = $con->prepare("DELETE FROM imagenes WHERE clave_producto = :clave");
        $dell->bindParam(':clave', $clave);
        $dell->execute();

        $dell = null;

        echo alerta('El productio ha sido eliminado',$pagina);
    }else{
        echo alerta('El producto no ha sido eliminado', $pagina);
    }

    $del = null;
    $con = null;


?>
</body>
</html>



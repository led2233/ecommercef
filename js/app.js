var firebaseConfig = {
    apiKey: "AIzaSyCUL3ToO39ArHtv1hK8Vf6Dvbv97rQ-uS4",
    authDomain: "ecommercef-ddd09.firebaseapp.com",
    databaseURL: "https://ecommercef-ddd09.firebaseio.com",
    projectId: "ecommercef-ddd09",
    storageBucket: "",
    messagingSenderId: "793522492118",
    appId: "1:793522492118:web:4d08d611362165affdaf77"
  };

  firebase.initializeApp(firebaseConfig);

  
const txtEmail = document.getElementById('correo');
const txtPassword = document.getElementById('pass');
const btnLogin = document.getElementById('login');

btnLogin.addEventListener('click', e => {
	const email = txtEmail.value;
	const pass = txtPassword.value;
	const auth = firebase.auth();

	const promesa = auth.signInWithEmailAndPassword(email,pass);
	promesa.catch(e => location.href = "admin/error.php");

});

firebase.auth().onAuthStateChanged(firebaseUser => {
	if (firebaseUser) {
			location.href="admin";
		}
});